import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Krejcda1Test {

    static Krejcda1 ob;

    @BeforeAll
    public static void setUp() throws Exception {
        Krejcda1Test.ob = new Krejcda1();
    }

    @Test
    public void test_fatorial_zero_one () {
        assertEquals(1, ob.factorial(0));
    }

    @Test
    public void test_factorial_one_one () {
        assertEquals(1, ob.factorial(1));
    }

    @Test
    public void test_factorial_n3_n6 () {
        assertEquals(6, ob.factorial(3));
    }

    @Test
    public void test_factorial_belowzero_zero () {
        assertEquals(0, ob.factorial(-2));
    }
}
