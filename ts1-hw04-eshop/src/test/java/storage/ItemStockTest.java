package storage;

import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import shop.Item;
import shop.StandardItem;
import storage.ItemStock;

public class ItemStockTest {

    @Mock StandardItem mocked;

    @Test
    public void constructor_null_success () {
        ItemStock stock = new ItemStock(null);

        assertEquals(null, stock.getItem());
        assertEquals(0, stock.getCount());
    }

    @Test
    public void constructor_basic_success () {
        ItemStock stock = new ItemStock(mocked);

        assertEquals(mocked, stock.getItem());
        assertEquals(0, stock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3})
    public void IncreaseItemCount_parameterized_success (int num) {
        ItemStock stock = new ItemStock(mocked);

        assertEquals(0, stock.getCount());

        stock.IncreaseItemCount(num);
        assertEquals(num, stock.getCount());
        stock.IncreaseItemCount(num);
        assertEquals(num * 2, stock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3})
    public void decreaseItemCount_parameterized_success (int num) {
        ItemStock stock = new ItemStock(mocked);

        assertEquals(0, stock.getCount());

        stock.decreaseItemCount(num);
        assertEquals(-num, stock.getCount());
        stock.decreaseItemCount(num);
        assertEquals(-num * 2, stock.getCount());
    }
}
