package shop;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import static shop.EShopController.archive;
import static shop.EShopController.purchaseShoppingCart;
import static shop.EShopController.storage;
import storage.NoItemInStorage;


public class EShopControllerTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();


    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void processTest_buyItem_success() throws NoItemInStorage {
        String addItem = "Item with ID 1 added to the shopping cart.\n";
        String statistics = "ITEM PURCHASE STATISTICS:\nITEM  Item   ID 1   NAME ItemA   CATEGORY CatA   PRICE 10.0   LOYALTY POINTS 0   HAS BEEN SOLD 2 TIMES\n";
        String contain = "STORAGE IS CURRENTLY CONTAINING:\nSTOCK OF ITEM:  Item   ID 1   NAME ItemA   CATEGORY CatA   PRICE 10.0   LOYALTY POINTS 0    PIECES IN STORAGE: 0\n";
        EShopController.startEShop();

        Item itemA = new StandardItem(1, "ItemA", 10, "CatA", 0);

        EShopController.storage.insertItems(itemA, 2);

        ShoppingCart cart = EShopController.newCart();

        // add and renove item
        cart.addItem(itemA);
        assertEquals(addItem, outputStreamCaptor.toString());
        outputStreamCaptor.reset();

        assertEquals(1, cart.getItemsCount());
        cart.removeItem(1);
        assertEquals(0, cart.getItemsCount());
        outputStreamCaptor.reset();

        // add 2 items
        cart.addItem(itemA);
        assertEquals(addItem, outputStreamCaptor.toString());
        outputStreamCaptor.reset();

        cart.addItem(itemA);
        assertEquals(addItem, outputStreamCaptor.toString());
        outputStreamCaptor.reset();

        assertEquals(2, cart.getItemsCount());
        assertEquals(20, cart.getTotalPrice());

        // buy cart
        EShopController.purchaseShoppingCart(cart, "User","Address");

        EShopController.archive.printItemPurchaseStatistics();
        assertEquals(statistics, outputStreamCaptor.toString());
        outputStreamCaptor.reset();

        EShopController.storage.printListOfStoredItems();
        assertEquals(contain, outputStreamCaptor.toString());
    }

}
