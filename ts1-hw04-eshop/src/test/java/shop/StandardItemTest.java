package shop;

import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class StandardItemTest {

    private static Stream<Arguments> providerEquals () {
        StandardItem itemA = new StandardItem(1, "A", 2f, "B", 3);
        StandardItem itemB = new StandardItem(1, "A", 2f, "B", 3);

        StandardItem itemC = new StandardItem(2, "A", 2f, "B", 3);
        StandardItem itemD = new StandardItem(1, "X", 2f, "B", 3);
        StandardItem itemE = new StandardItem(1, "A", 4f, "B", 3);
        StandardItem itemF = new StandardItem(1, "A", 2f, "X", 3);
        StandardItem itemG = new StandardItem(1, "A", 2f, "B", 8);

        return Stream.of(
                Arguments.of(itemA,itemA,true),
                Arguments.of(itemA,itemB,true),
                Arguments.of(itemA,null,false),
                Arguments.of(itemA,itemC,false),
                Arguments.of(itemA,itemD,false),
                Arguments.of(itemA,itemE,false),
                Arguments.of(itemA,itemF,false),
                Arguments.of(itemA,itemG,false)
        );
    }

    @Test
    public void constructor_basic_success() {
        int id = 10;
        String name = "test";
        float price = 1.2f;
        String category = "cattest";
        int points = 99;

        StandardItem item = new StandardItem(id, name, price, category, points);

        assertEquals(item.getID(), id);
        assertEquals(item.getName(), name);
        assertEquals(item.getPrice(), price);
        assertEquals(item.getCategory(), category);
        assertEquals(item.getLoyaltyPoints(), points);
    }

    @Test
    public void copy_basic_success () {
        StandardItem original = new StandardItem(1, "A", 2f, "B", 3);

        StandardItem copy = original.copy();

        assertEquals(original.getID(), copy.getID());
        assertEquals(original.getName(), copy.getName());
        assertEquals(original.getPrice(), copy.getPrice());
        assertEquals(original.getCategory(), copy.getCategory());
        assertEquals(original.getLoyaltyPoints(), copy.getLoyaltyPoints());
    }

    @ParameterizedTest(name = "#{index} - should equals ={3}")
    @MethodSource("providerEquals")
    public void equals_parameterized_boolean (StandardItem itemA, StandardItem itemB, boolean equals) {
        assertEquals(itemA.equals(itemB), equals);
    }
}
