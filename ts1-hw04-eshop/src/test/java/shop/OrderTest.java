package shop;

import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrderTest {

    String name = "Name";
    String address = "Address";
    int state = 1;
    int defaultState = 0;
    ArrayList<Item> items = new ArrayList<>();

    @Test
    public void constructor_fourParams_success () {
        ShoppingCart cart = mock(ShoppingCart.class);
        when(cart.getCartItems()).thenReturn(items);

        Order ord = new Order(cart, name, address, state);

        assertEquals(items, ord.getItems());
        assertEquals(name, ord.getCustomerName());
        assertEquals(address, ord.getCustomerAddress());
        assertEquals(state, ord.getState());
    }

    @Test
    public void constructor_threeParams_success () {
        ShoppingCart cart = mock(ShoppingCart.class);
        when(cart.getCartItems()).thenReturn(items);

        Order ord = new Order(cart, name, address);

        assertEquals(items, ord.getItems());
        assertEquals(name, ord.getCustomerName());
        assertEquals(address, ord.getCustomerAddress());
        assertEquals(defaultState, ord.getState());
    }

    @Test
    public void constructor_null_fail () {
        Assertions.assertThrows(NullPointerException.class, () -> {
            new Order(null, name, address);
        });
    }
}
