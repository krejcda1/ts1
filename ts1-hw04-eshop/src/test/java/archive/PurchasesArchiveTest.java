package archive;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import shop.Item;
import shop.Order;

public class PurchasesArchiveTest {

    static int itemKey = 99;
    static int itemNotKey = 9;
    static int soldCount = 10;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private HashMap getMockArchive () {
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        when(entry.getCountHowManyTimesHasBeenSold()).thenReturn(soldCount);
        when(entry.toString()).thenReturn("Test");

        Collection<ItemPurchaseArchiveEntry> collection = new ArrayList<ItemPurchaseArchiveEntry>();
        collection.add(entry);

        HashMap mockArchive = mock(HashMap.class);
        when(mockArchive.containsKey(itemKey)).thenReturn(true);
        when(mockArchive.containsKey(itemNotKey)).thenReturn(false);
        when(mockArchive.get(itemKey)).thenReturn(entry);
        when(mockArchive.values()).thenReturn(collection);

        return mockArchive;
    }

    private static Stream<Arguments> providerEquals () {
        Item mockExist = mock(Item.class);
        when(mockExist.getID()).thenReturn(itemKey);

        Item mockNonexist = mock(Item.class);
        when(mockNonexist.getID()).thenReturn(itemNotKey);

        return Stream.of(
            Arguments.of(mockExist, soldCount),
            Arguments.of(mockNonexist, 0)
        );
    }

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void printItemPurchaseStatistics_empty_none() {
        String expect = "ITEM PURCHASE STATISTICS:\n";

        PurchasesArchive archive = new PurchasesArchive();

        archive.printItemPurchaseStatistics();
        assertEquals(expect, outputStreamCaptor.toString());
    }

    @Test
    public void printItemPurchaseStatistics_oneItem_value() {
        String expect = "ITEM PURCHASE STATISTICS:\nTest\n";

        ArrayList orders = mock(ArrayList.class);
        PurchasesArchive archive = new PurchasesArchive(getMockArchive(), orders);

        archive.printItemPurchaseStatistics();
        assertEquals(expect, outputStreamCaptor.toString());
    }

    @ParameterizedTest
    @MethodSource("providerEquals")
    public void getHowManyTimesHasBeenItemSold_parameterized_existAndDont(Item item, int count) {
        ArrayList orders = mock(ArrayList.class);
        PurchasesArchive archive = new PurchasesArchive(getMockArchive(), orders);

        assertEquals(count, archive.getHowManyTimesHasBeenItemSold(item));
    }
}
