import cz.cvut.fel.ts1.Calculator;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    static Calculator calc;

    @BeforeAll
    public static void setUp() throws Exception {
        calc = new Calculator();
    }

    @Test
    @DisplayName("Secti 10 + 11 = 21")
    public void test_add_positiveNumbers_int () {
        // arrange
        int a = 10;
        int b = 11;
        int exp = 21;
        // act
        int out = calc.add(a, b);
        // assert
        assertEquals(out, exp);
    }

    @Test
    @DisplayName("Odecti 12 - 11 = 1")
    public void test_subtract_positiveNumbers_int () {
        // arrange
        int a = 12;
        int b = 11;
        int exp = 1;
        // act
        int out = calc.subtract(a, b);
        // assert
        assertEquals(out, exp);
    }

    @Test
    @DisplayName("Vynasob 5 * 4 = 20")
    public void test_multiply_positiveNumbers_int () {
        // arrange
        int a = 5;
        int b = 4;
        int exp = 20;
        // act
        int out = calc.multiply(a, b);
        // assert
        assertEquals(out, exp);
    }

    @Test
    @DisplayName("Secti 10 + 2 = 5")
    public void test_divide_positiveNumbers_int () {
        // arrange
        int a = 10;
        int b = 2;
        int exp = 5;
        // act
        int out = calc.divide(a, b);
        // assert
        assertEquals(out, exp);
    }

    @Test
    @DisplayName("Secti 10 / 0 = ArithmeticException")
    public void test_divide_zero_exeption () {
        // arrange
        int a = 10;
        int b = 0;
        // act
        Assertions.assertThrows(ArithmeticException.class, () -> {
           calc.divide(a, b);
         });
    }
}